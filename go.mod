module findbook

go 1.18

require (
	github.com/Unknwon/goconfig v1.0.0
	gorm.io/driver/mysql v1.5.1
	gorm.io/gorm v1.25.1
	jihulab.com/rickyngu/elkproducer v1.0.7
)

require (
	github.com/elastic/elastic-transport-go/v8 v8.0.0-20230201152525-7be14259265a // indirect
	github.com/elastic/go-elasticsearch/v8 v8.4.0-alpha.1.0.20230221175927-bc507c43e0f7 // indirect
	github.com/go-sql-driver/mysql v1.7.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/smartystreets/goconvey v1.8.0 // indirect
)
