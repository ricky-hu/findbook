package main

import (
	"errors"
	"flag"
	"fmt"
	"gorm.io/gorm"
	"io/ioutil"
	"jihulab.com/rickyngu/elkproducer"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

var Es elkproducer.Client
var DB *gorm.DB
var FilenameDB string
var Filename string

const (
	FileNameA      string = "RAR"
	FileNameB      string = "./cdr"
	FileNameC      string = "./ps"
	FileNameD      string = "./eps"
	FileNameE      string = "./fbd"
	FileNameF      string = "./PS"
	FileNameH      string = "./s92"
	FileNameI      string = "./rar"
	FileNameJ      string = "./jpg"
	FileNameK      string = "./doc"
	FileNameL      string = "./FBD"
	FileNameM      string = "./s10"
	FileNameN      string = "./xls"
	FileNameO      string = "./bak"
	FileNameP      string = "./RED"
	FileNameQ      string = "./bmp"
	FileNameR      string = "./tif"
	FileNameX      string = "./TXT"
	GO_TIME_FORMAT        = "2006-01-02 15:04"
)

//func init() {
//	//var FilePath string
//	FilePath := os.Getenv("FILEPATH")
//	if FilePath == "" {
//		FilePath = "config.conf"
//	}
//	config, err := goconfig.LoadConfigFile(FilePath) //加载配置文件
//	if err != nil {
//		fmt.Println("get config file error", err)
//		os.Exit(-1)
//	}
//	glob, _ := config.GetSection("message") //读取全部mysql配置
//	db, err := gorm.Open(mysql.New(mysql.Config{
//		DSN:                       glob["username"] + ":" + glob["password"] + "@tcp(" + glob["ip"] + ":" + glob["port"] + ")/" + glob["dbname"] + "?charset=utf8&parseTime=True&loc=Local", // DSN data source name
//		DefaultStringSize:         256,                                                                                                                                                      // string 类型字段的默认长度
//		DisableDatetimePrecision:  true,                                                                                                                                                     // 禁用 datetime 精度，MySQL 5.6 之前的数据库不支持
//		DontSupportRenameIndex:    true,                                                                                                                                                     // 重命名索引时采用删除并新建的方式，MySQL 5.7 之前的数据库和 MariaDB 不支持重命名索引
//		DontSupportRenameColumn:   true,                                                                                                                                                     // 用 `change` 重命名列，MySQL 8 之前的数据库和 MariaDB 不支持重命名列
//		SkipInitializeWithVersion: false,                                                                                                                                                    // 根据当前 MySQL 版本自动配置
//	}), &gorm.Config{})
//	if err != nil {
//		return
//	}
//	DB = db
//	cfg := elkproducer.Config{
//		Addresses: []string{
//			glob["elkurl"],
//		},
//		Username: glob["elkusername"],
//		Password: glob["elkpassword"],
//	}
//	configelk := elkproducer.ESConfig{
//		ESConf: cfg,
//		//IndexType: "log",
//		DebugMode: false,
//		Index:     glob["elkuserindex"],
//	}
//	es, err := elkproducer.NewClient(configelk)
//	if err != nil {
//		fmt.Println("first elkproducer.NewClient", err)
//	} else {
//		Es = es
//	}
//}

//type Book struct {
//	Book      string `json:"book" gorm:"book"`
//	Sort      string `json:"sort" gorm:"sort"`
//	Author    string `json:"author" gorm:"author"`
//	Press     string `json:"press" gorm:"press"`
//	ISBN      string `json:"ISBN" gorm:"ISBN"`
//	Pressdate string `json:"pressdate" gorm:"pressdate"`
//	Date      string `json:"date" gorm:"date"`
//	Status    int    `json:"status" gorm:"status"`
//	Filepath  string `json:"filepath" gorm:"filepath"`
//	Remark    string `json:"remark" gorm:"remark"`
//	Bkid      int    `json:"bkid" gorm:"bkid"`
//}

type NBook struct {
	Book      string `json:"book" gorm:"book"`
	Sort      string `json:"sort" gorm:"sort"`
	Author    string `json:"author" gorm:"author"`
	Press     string `json:"press" gorm:"press"`
	ISBN      string `json:"ISBN" gorm:"ISBN"`
	Pressdate string `json:"pressdate" gorm:"pressdate"`
	Date      string `json:"date" gorm:"date"`
	Way       int    `json:"way" gorm:"way"`
	Status    int    `json:"status" gorm:"status"`
	Filepath  string `json:"filepath" gorm:"filepath"`
	Remark    string `json:"remark" gorm:"remark"`
	Bkid      int    `json:"bkid" gorm:"bkid"`
	Nid       string `json:"nid" gorm:"nid"`
}

type In struct {
	Index Index `json:"index"`
}

type Index struct {
	Index string `json:"_index"`
}

var File *os.File

func main() {
	p := flag.String("p", "", "目录")
	o := flag.Int("o", 0, "类型")
	s := flag.Int64("s", 1048576, "大小")
	d := flag.String("d", "find-dir", "目的地目录")
	y := flag.String("y", "/data_15", "压缩目录")
	flag.Parse()
	fmt.Println(*p)
	strings.Split(*p, "/")
	FilenameDB = strings.Split(*p, "/")[1] + "-RAR"
	Filename = strings.Split(*p, "/")[1] + "-ZIP"
	if *o == 1 {
		err := WalkDirNid(*p, *o, *s)
		fmt.Println(err)
	} else if *o == 2 {
		err := WalkDirBook(*p, *o, *s)
		fmt.Println(err)
	} else if *o == 3 {
		err := WalkDirNew(*p, *o, *s)
		fmt.Println(err)
	} else if *o == 4 {
		err := WalkDirRAR(*p, *o, *d)
		fmt.Println(err)
	} else if *o == 5 {
		err := WalkDirMVRAR(*p, *o, *s, *d)
		fmt.Println(err)
	} else if *o == 6 {
		file, errs := os.OpenFile(*d, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
		if errs != nil {
			fmt.Printf("Open file error. err=%s", errs)
		}
		File = file
		defer file.Close()
		_, err := WalkDir(*p, []string{})
		fmt.Println(err)
	} else if *o == 7 {
		DoWalkDir()
		Write("tongji", []byte("目录数"+strconv.Itoa(int(M))))
		Write("tongji", []byte("\n"))
		Write("tongji", []byte("文件数"+strconv.Itoa(int(W))))
		Write("tongji", []byte("\n"))
		Write("tongji", []byte("总空间"+strconv.Itoa(int(F))))
		Write("tongji", []byte("\n"))
	} else if *o == 8 {
		err := WalkDirLocal(*p)
		fmt.Println(err)
	} else {
		err := WalkDirAll(*p, *d, *y)
		fmt.Println(err)
	}

}

var M int64
var F int64
var W int64

func DoWalkDir() {
	s := []string{"sd", "se", "sf", "sg", "sh", "si", "sj", "sk1", "sk2", "sk3", "sk4", "sk5", "sk6", "sk7", "sk8", "sl"}
	for _, v := range s {
		file, errs := os.OpenFile("data18rb"+v+".txt", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
		if errs != nil {
			fmt.Printf("Open file error. err=%s", errs)
		}
		File = file
		defer file.Close()
		WalkDir("/data_18/rootbook/"+v, []string{})
	}
}

func WalkDir(dirPth string, s []string) ([]string, error) {
	gn := " Z:" + strings.Replace(dirPth, "/", "\\", -1) + " " + "的目录"
	Writes([]byte(gn))
	Writes([]byte("\n"))
	rd, err := ioutil.ReadDir(dirPth) //读取目录的所有内容
	if err != nil {
		M++
		fmt.Println("read dir fail:", err)
		return s, err
	}
	for _, fi := range rd { //遍历是目录还是文件，目录就记录下来，加入到一个数组里
		if fi.IsDir() {
			fpa := time.Now().Format(GO_TIME_FORMAT) + " <DIR> " + fi.Name()
			Writes([]byte(fpa))
			Writes([]byte("\n"))
			s = append(s, dirPth+"/"+fi.Name())
			//fmt.Println(fi.Name())
		} else {
			W++
			F += fi.Size()
			fa := time.Now().Format(GO_TIME_FORMAT) + " " + strconv.Itoa(int(fi.Size())) + " " + fi.Name()
			Writes([]byte(fa))
			Writes([]byte("\n"))
			//fmt.Println(fi.Name())
		}
	}
	for _, v := range s { //遍历上面的数组
		WalkDir(v, []string{})
	}
	return s, nil

}

func WalkDirNid(dirPth string, o int, s int64) error {
	//suffix = strings.ToUpper(suffix) //忽略后缀匹配的大小写
	var i int
	var size int64
	err := filepath.Walk(dirPth, func(filename string, fi os.FileInfo, err error) error { //遍历目录
		//if err != nil { //忽略错误
		//    return err
		//}
		if fi.IsDir() { // 忽略目录
			size = 0
			i = 0
			book, _ := ESnid(MatchChineseForStr(fi.Name(), 2))
			if book.Nid == "" {
				files, err := ioutil.ReadDir(filename)
				if err != nil {
					panic(err)
				}
				for _, f := range files {
					size += f.Size()
				}
				if size > s {
					//fmt.Println("1111111")
					i = 1
				}
				Write(Filename, []byte(filename))
				Write(Filename, []byte("\n"))
			} else {
				book.Filepath = filename
				err := UpdateDB(book)
				if err != nil {
					fmt.Println(err)
				}
				Write(FilenameDB, []byte(fi.Name()))
				Write(FilenameDB, []byte("\n"))
			}
			return nil
		}
		if i == 1 {
			book, _ := ESbook(MatchChineseForStr(fi.Name(), 2))
			if book.Nid == "" {
				Write(Filename, []byte(filename))
				Write(Filename, []byte("\n"))
			} else {
				book.Filepath = filename
				err := UpdateDB(book)
				if err != nil {
					fmt.Println(err)
				}
				Write(FilenameDB, []byte(fi.Name()))
				Write(FilenameDB, []byte("\n"))
			}
		}

		//for _, v := range data() {
		//	suffix := strings.ToUpper(v) //忽略后缀匹配的大小写
		//	if strings.HasSuffix(strings.ToUpper(fi.Name()), suffix) && fi.Size() > 1024 {
		//		err1 := Write(suffix[1:], []byte(filename))
		//		err2 := Write(suffix[1:], []byte("\n"))
		//		//files = append(files, filename)
		//		fmt.Println(filename, err1, err2)
		//	}
		//}
		return nil
	})
	return err
}

func WalkDirBook(dirPth string, o int, s int64) error {
	//suffix = strings.ToUpper(suffix) //忽略后缀匹配的大小写
	var i int
	var size int64
	err := filepath.Walk(dirPth, func(filename string, fi os.FileInfo, err error) error { //遍历目录
		//if err != nil { //忽略错误
		//    return err
		//}
		if fi.IsDir() { // 忽略目录
			size = 0
			i = 0
			book, _ := ESbook(MatchChineseForStr(fi.Name(), 2))
			if book.Nid == "" {
				files, err := ioutil.ReadDir(filename)
				if err != nil {
					panic(err)
				}
				for _, f := range files {
					size += f.Size()
				}
				if size > s {
					//fmt.Println("1111111")
					i = 1
				}
				Write(Filename, []byte(filename))
				Write(Filename, []byte("\n"))
			} else {
				book.Filepath = filename
				err := UpdateDB(book)
				if err != nil {
					fmt.Println(err)
				}
				Write(FilenameDB, []byte(fi.Name()))
				Write(FilenameDB, []byte("\n"))
			}
			return nil
		}
		if i == 1 {
			book, _ := ESbook(MatchChineseForStr(fi.Name(), 2))
			if book.Nid == "" {
				Write(Filename, []byte(filename))
				Write(Filename, []byte("\n"))
			} else {
				book.Filepath = filename
				err := UpdateDB(book)
				if err != nil {
					fmt.Println(err)
				}
				Write(FilenameDB, []byte(fi.Name()))
				Write(FilenameDB, []byte("\n"))
			}
		}

		//for _, v := range data() {
		//	suffix := strings.ToUpper(v) //忽略后缀匹配的大小写
		//	if strings.HasSuffix(strings.ToUpper(fi.Name()), suffix) && fi.Size() > 1024 {
		//		err1 := Write(suffix[1:], []byte(filename))
		//		err2 := Write(suffix[1:], []byte("\n"))
		//		//files = append(files, filename)
		//		fmt.Println(filename, err1, err2)
		//	}
		//}
		return nil
	})
	return err
}

func WalkDirNew(dirPth string, o int, s int64) error {
	//suffix = strings.ToUpper(suffix) //忽略后缀匹配的大小写
	var i int
	size := make(map[string]string)
	err := filepath.Walk(dirPth, func(filename string, fi os.FileInfo, err error) error { //遍历目录
		//if err != nil { //忽略错误
		//    return err
		//}
		if fi.IsDir() { // 忽略目录
			//size = 0
			i = 0
			if strings.ToUpper(fi.Name()) == "IFR" {
				size = make(map[string]string)
				i = 1
			}
			if strings.ToUpper(fi.Name()) == "SEP" {
				i = 2
			}
			return nil
		}
		if i == 1 {
			ms := strings.TrimSuffix(fi.Name(), ".IFR")
			if strings.HasSuffix(strings.ToUpper(fi.Name()), "IFR") && len(ms) == 4 {
				size[ms] = ms
				fmt.Println("nid is", ms)
			}
		} else if i == 2 {
			value, ok := size[fi.Name()[:4]]
			if ok {
				err := UpdateDB(NBook{Nid: value})
				if err != nil {
					fmt.Println(err)
				} else {
					Write(FilenameDB, []byte(fi.Name()))
					Write(FilenameDB, []byte("\n"))
					fmt.Println("find book success nid is", value)
				}
			} else {
				Write(Filename, []byte(filename))
				Write(Filename, []byte("\n"))
				fmt.Println("find book faild nid is ", value)
			}
		}
		//for _, v := range data() {
		//	suffix := strings.ToUpper(v) //忽略后缀匹配的大小写
		//	if strings.HasSuffix(strings.ToUpper(fi.Name()), suffix) && fi.Size() > 1024 {
		//		err1 := Write(suffix[1:], []byte(filename))
		//		err2 := Write(suffix[1:], []byte("\n"))
		//		//files = append(files, filename)
		//		fmt.Println(filename, err1, err2)
		//	}
		//}
		return nil
	})
	return err
}

func WalkDirRAR(dirPth string, o int, d string) error {
	//suffix := strings.ToUpper(".rar") //忽略后缀匹配的大小写
	var filepathname string
	err := filepath.Walk(dirPth, func(filename string, fi os.FileInfo, err error) error { //遍历目录
		//if err != nil { //忽略错误
		//    return err
		//}
		if fi.IsDir() { // 忽略目录
			//size = 0
			filepathname = filename
			return nil
		}
		suffixrar := strings.ToUpper(".rar")    //忽略后缀匹配的大小写
		suffixzip := strings.ToUpper(".zip")    //忽略后缀匹配的大小写
		suffixtar := strings.ToUpper(".tar.gz") //忽略后缀匹配的大小写
		s := filepathname[:strings.LastIndex(filename, "/")]
		ds := d + s
		fmt.Println("ds", ds)
		MakeFile(ds)
		if strings.HasSuffix(strings.ToUpper(fi.Name()), suffixrar) {
			zipname := strings.TrimSuffix(filename, ".rar")
			if IsDir(zipname) == true {
				MVFile(filename, ds)
			} else {
				if RAR(filename, zipname) != nil {
					fmt.Println("RAR(filename,ds)", err)
				} else {
					MVFile(filename, ds)
				}
			}
		} else if strings.HasSuffix(strings.ToUpper(fi.Name()), suffixzip) {
			zipname := strings.Split(filename, ".")
			if IsDir(zipname[0]) == true {
				MVFile(filename, ds)
			} else {
				if ZIP(filename, zipname[0]) != nil {
					fmt.Println("ZIP(ds,filename)", err)
				} else {
					MVFile(filename, ds)
				}
			}
		} else if strings.HasSuffix(strings.ToUpper(fi.Name()), suffixtar) {
			zipname := strings.Split(filename, ".")
			if IsDir(zipname[0]) == true {
				MVFile(filename, ds)
			} else {
				if TAR(filename, zipname[0]) != nil {
					fmt.Println("TAR(ds,filename)", err)
				} else {
					MVFile(filename, ds)
				}
			}
		}
		return nil
	})
	return err
}

func WalkDirMVRAR(dirPth string, o int, s int64, d string) error {
	//suffix := strings.ToUpper(".rar") //忽略后缀匹配的大小写
	var filepathname string
	err := filepath.Walk(dirPth, func(filename string, fi os.FileInfo, err error) error { //遍历目录
		//if err != nil { //忽略错误
		//    return err
		//}
		if fi.IsDir() { // 忽略目录
			//size = 0
			filepathname = filename
			return nil
		}
		suffix := strings.ToUpper(".rar")    //忽略后缀匹配的大小写
		suffixzip := strings.ToUpper(".zip") //忽略后缀匹配的大小写
		if strings.HasSuffix(strings.ToUpper(fi.Name()), suffix) {
			ds := filepathname[:strings.LastIndex(filename, "/")]
			fmt.Println("filenamerar", filename, filepathname, ds)
			zipname := strings.TrimSuffix(filename, ".rar")
			if IsDir(zipname) == true {
				fmt.Println("111")
				if IsDir(d+zipname) == false {
					fmt.Println("222")
					cmd := exec.Command("mkdir", "-p", d+ds)
					out, err := cmd.CombinedOutput()
					if err != nil {
						fmt.Println("333")
						Write("mk-err", []byte(err.Error()))
						Write("mk-err", []byte("\n"))
						Write("mk-err-msg", out)
						Write("mk-err-msg", []byte("\n"))
						return nil
						fmt.Printf("combined out:\n%s\n", string(out))
						log.Fatalf("cmd.Run() failed with %s\n", err)
					} else {
						fmt.Println("444")
						cmd := exec.Command("mv", filename, d+ds)
						out, err := cmd.CombinedOutput()
						if err != nil {
							Write("mk-err", []byte(err.Error()))
							Write("mk-err", []byte("\n"))
							Write("mk-err-msg", out)
							Write("mk-err-msg", []byte("\n"))
							return nil
						} else {
							fmt.Println("555")
							fmt.Printf("combined out:\n%s\n", string(out))

							Write("succ", []byte(filename))
							Write("succ", []byte("\n"))
						}
					}
				} else {
					fmt.Println("666")
					cmd := exec.Command("mv", "-f", filename, d+ds)
					out, err := cmd.CombinedOutput()
					if err != nil {
						Write("mk-err", []byte(err.Error()))
						Write("mk-err", []byte("\n"))
						Write("mk-err-msg", out)
						Write("mk-err-msg", []byte("\n"))
						return nil
					} else {
						fmt.Println("777")
						fmt.Printf("combined out:\n%s\n", string(out))

						Write("succ", []byte(filename))
						Write("succ", []byte("\n"))
					}
				}
				return nil
			}
		} else if strings.HasSuffix(strings.ToUpper(fi.Name()), suffixzip) {
			ds := filepathname[:strings.LastIndex(filename, "/")]
			fmt.Println("filenamezip1111", filename, filepathname, ds)
			zipname := strings.Split(filename, ".")
			if IsDir(zipname[0]) == true {
				fmt.Println("888")
				if IsDir(d+zipname[0]) == false {
					fmt.Println("999")
					cmd := exec.Command("mkdir", "-p", d+ds)
					out, err := cmd.CombinedOutput()
					if err != nil {
						fmt.Println("10-10")
						Write("mk-err", []byte(err.Error()))
						Write("mk-err", []byte("\n"))
						Write("mk-err-msg", out)
						Write("mk-err-msg", []byte("\n"))
						return nil
						fmt.Printf("combined out:\n%s\n", string(out))
						log.Fatalf("cmd.Run() failed with %s\n", err)
					} else {
						fmt.Println("11-11")
						cmd := exec.Command("mv", filename, d+ds)
						out, err := cmd.CombinedOutput()
						if err != nil {
							fmt.Println("12-12")
							Write("mk-err", []byte(err.Error()))
							Write("mk-err", []byte("\n"))
							Write("mk-err-msg", out)
							Write("mk-err-msg", []byte("\n"))
							return nil
						} else {
							fmt.Println("13-13")
							fmt.Printf("combined out:\n%s\n", string(out))

							Write("succ", []byte(filename))
							Write("succ", []byte("\n"))
						}
					}
				} else {
					fmt.Println("16-16")
					cmd := exec.Command("mv", "-f", filename, d+ds)
					out, err := cmd.CombinedOutput()
					if err != nil {
						fmt.Println("14-14")
						Write("mk-err", []byte(err.Error()))
						Write("mk-err", []byte("\n"))
						Write("mk-err-msg", out)
						Write("mk-err-msg", []byte("\n"))
						return nil
					} else {
						fmt.Println("15-15")
						fmt.Printf("combined out:\n%s\n", string(out))

						Write("succ", []byte(filename))
						Write("succ", []byte("\n"))
					}
				}
				return nil
			}
		}
		return nil
	})
	return err
}

func WalkDirLocal(dirPth string) error {
	var filepathname string
	err := filepath.Walk(dirPth, func(filename string, fi os.FileInfo, err error) error { //遍历目录
		//if err != nil { //忽略错误
		//    return err
		//}
		fmt.Println("filename", filename)
		if fi.IsDir() { // 忽略目录
			filepathname = filename
			return nil
		}
		suffixrar := strings.ToUpper(".rar")    //忽略后缀匹配的大小写
		suffixzip := strings.ToUpper(".zip")    //忽略后缀匹配的大小写
		suffixtar := strings.ToUpper(".tar.gz") //忽略后缀匹配的大小写
		s := filepathname[:strings.LastIndex(filename, "/")]

		if strings.HasSuffix(strings.ToUpper(fi.Name()), suffixrar) {
			zipname := strings.TrimSuffix(filename, ".rar")
			if IsDir(zipname) == true || IsFile(zipname) == true {
				//MVFile(filename, ds)
			} else {
				cmd := exec.Command("unrar", "x", "-o-", "-y", ""+filename, s+"/")
				out, err := cmd.CombinedOutput()
				if err != nil {
					Write("err", []byte(err.Error()))
					Write("err", []byte("\n"))
					Write("err-msg", out)
					Write("err-msg", []byte("\n"))
					Write("err-book", []byte(s))
					Write("err-book", []byte("\n"))
					return nil
					fmt.Printf("combined out:\n%s\n", string(out))
					log.Fatalf("cmd.Run() failed with %s\n", err)
				}
			}
		} else if strings.HasSuffix(strings.ToUpper(fi.Name()), suffixzip) {
			zipname := strings.Split(filename, ".")
			if IsDir(zipname[0]) == true || IsFile(zipname[0]) == true {
				//MVFile(filename, ds)
			} else {
				cmds := exec.Command("unzip", "-n", "-d", s, filename)
				out, err := cmds.CombinedOutput()
				if err != nil {
					Write("err", []byte(err.Error()))
					Write("err", []byte("\n"))
					Write("err-msg", out)
					Write("err-msg", []byte("\n"))
					Write("err-book", []byte(s))
					Write("err-book", []byte("\n"))
					return nil
					fmt.Printf("combined out:\n%s\n", string(out))
					log.Fatalf("cmd.Run() failed with %s\n", err)
				}
			}
		} else if strings.HasSuffix(strings.ToUpper(fi.Name()), suffixtar) {
			zipname := strings.Split(filename, ".")
			if IsDir(zipname[0]) == true || IsFile(zipname[0]) == true {
				//MVFile(filename, ds)
			} else {
				if TAR(filename, s) != nil {
					fmt.Println("TAR(ds,filename)", err)
				}
			}
		}
		return nil
	})
	return err
}

func WalkDirAll(dirPth string, d string, y string) error {
	var filepathname string
	err := filepath.Walk(dirPth, func(filename string, fi os.FileInfo, err error) error { //遍历目录
		//if err != nil { //忽略错误
		//    return err
		//}
		fmt.Println("filename", filename)
		if fi.IsDir() { // 忽略目录
			filepathname = filename
			return nil
		}
		suffixrar := strings.ToUpper(".rar")    //忽略后缀匹配的大小写
		suffixzip := strings.ToUpper(".zip")    //忽略后缀匹配的大小写
		suffixtar := strings.ToUpper(".tar.gz") //忽略后缀匹配的大小写
		s := filepathname[:strings.LastIndex(filename, "/")]
		ds := y + s
		dds := d + s
		fmt.Println("ds", dds, ds)
		MakeFile(ds)
		MakeFile(dds)
		if strings.HasSuffix(strings.ToUpper(fi.Name()), suffixrar) {
			zipname := strings.TrimSuffix(filename, ".rar")
			if IsDir(zipname) == true || IsFile(zipname) == true {
				MVFile(filename, ds)
			} else {
				cmd := exec.Command("unrar", "x", "-o-", "-y", ""+filename, dds+"/")
				out, err := cmd.CombinedOutput()
				if err != nil {
					Write("err", []byte(err.Error()))
					Write("err", []byte("\n"))
					Write("err-msg", out)
					Write("err-msg", []byte("\n"))
					Write("err-book", []byte(s))
					Write("err-book", []byte("\n"))
					return nil
					fmt.Printf("combined out:\n%s\n", string(out))
					log.Fatalf("cmd.Run() failed with %s\n", err)

				} else {
					fmt.Printf("combined out:\n%s\n", string(out))
					cmd := exec.Command("ln", "-s", d+zipname+"/", zipname)
					out, err := cmd.CombinedOutput()
					if err != nil {
						Write("err", []byte(err.Error()))
						Write("err", []byte("\n"))
						Write("ln-msg", out)
						Write("ln-msg", []byte("\n"))
						Write("ln-book", []byte(s))
						Write("ln-book", []byte("\n"))
						return nil
						fmt.Printf("combined out:\n%s\n", string(out))
						log.Fatalf("cmd.Run() failed with %s\n", err)
					} else {
						fmt.Printf("combined out:\n%s\n", string(out))
						MVFile(filename, ds)
						Write("ln", []byte(s))
						Write("ln", []byte("\n"))
						return nil
					}
					Write(FilenameDB, []byte(s))
					Write(FilenameDB, []byte("\n"))
					return nil
				}
				//go LNS(d+zipname, zipname)
				//MVFile(filename, ds)

			}
		} else if strings.HasSuffix(strings.ToUpper(fi.Name()), suffixzip) {
			zipname := strings.Split(filename, ".")
			if IsDir(zipname[0]) == true || IsFile(zipname[0]) == true {
				MVFile(filename, ds)
			} else {
				fmt.Println("zip", filename, dds)
				cmds := exec.Command("unzip", "-n", "-d", dds, filename)
				out, err := cmds.CombinedOutput()
				if err != nil {
					Write("err", []byte(err.Error()))
					Write("err", []byte("\n"))
					Write("err-msg", out)
					Write("err-msg", []byte("\n"))
					Write("err-book", []byte(s))
					Write("err-book", []byte("\n"))
					return nil
					fmt.Printf("combined out:\n%s\n", string(out))
					log.Fatalf("cmd.Run() failed with %s\n", err)
				} else {
					fmt.Printf("combined out:\n%s\n", string(out))
					cmd := exec.Command("ln", "-s", d+zipname[0]+"/", zipname[0])
					out, err := cmd.CombinedOutput()
					if err != nil {
						Write("err", []byte(err.Error()))
						Write("err", []byte("\n"))
						Write("ln-msg", out)
						Write("ln-msg", []byte("\n"))
						Write("ln-book", []byte(s))
						Write("ln-book", []byte("\n"))
						return nil
						fmt.Printf("combined out:\n%s\n", string(out))
						log.Fatalf("cmd.Run() failed with %s\n", err)
					} else {
						fmt.Printf("combined out:\n%s\n", string(out))
						MVFile(filename, ds)
						Write("ln", []byte(s))
						Write("ln", []byte("\n"))
						return nil
					}
					Write(Filename, []byte(s))
					Write(Filename, []byte("\n"))
					return nil
				}
				//go LNS(d+zipname[0], zipname[0])
				//MVFile(filename, ds)
			}
		} else if strings.HasSuffix(strings.ToUpper(fi.Name()), suffixtar) {
			zipname := strings.Split(filename, ".")
			if IsDir(zipname[0]) == true || IsFile(zipname[0]) == true {
				MVFile(filename, ds)
			} else {
				fmt.Println("tar", filename, dds)
				if TAR(filename, dds) != nil {
					fmt.Println("TAR(ds,filename)", err)
				} else {
					fmt.Println("pop", d+zipname[0], zipname[0])

					//go LNS(d+zipname[0], zipname[0])
					//MVFile(filename, ds)
					cmd := exec.Command("ln", "-s", d+zipname[0]+"/", zipname[0])
					out, err := cmd.CombinedOutput()
					if err != nil {
						Write("err", []byte(err.Error()))
						Write("err", []byte("\n"))
						Write("ln-msg", out)
						Write("ln-msg", []byte("\n"))
						Write("ln-book", []byte(s))
						Write("ln-book", []byte("\n"))
						return nil
						fmt.Printf("combined out:\n%s\n", string(out))
						log.Fatalf("cmd.Run() failed with %s\n", err)
					} else {
						fmt.Printf("combined out:\n%s\n", string(out))
						MVFile(filename, ds)
						Write("ln", []byte(s))
						Write("ln", []byte("\n"))
						return nil
					}
				}
			}
		}
		return nil
	})
	return err
}

func Dfile(filepathname string) error {
	fmt.Println("sljaflajsfd", filepathname)
	err := filepath.Walk(filepathname, func(filename string, fi os.FileInfo, err error) error { //遍历目录
		if fi.IsDir() { // 忽略目录
			filepathname = filename
			return nil
		}
		suffix := strings.ToUpper(".rar")    //忽略后缀匹配的大小写
		suffixzip := strings.ToUpper(".zip") //忽略后缀匹配的大小写
		if strings.HasSuffix(strings.ToUpper(fi.Name()), suffix) {
			fmt.Println("ddddd", filename)
			zipname := strings.TrimSuffix(filename, ".rar")
			if IsDir(zipname) == true {
				fmt.Println("zipname[55555]", zipname[0])
				Dfile(zipname)
				return nil
			}
			cmd := exec.Command("unrar", "x", "-o-", "-y", ""+filename, zipname+"/")
			out, err := cmd.CombinedOutput()
			if err != nil {
				Write("err", []byte(err.Error()))
				Write("err", []byte("\n"))
				Write("err-msg", out)
				Write("err-msg", []byte("\n"))
				return nil
				fmt.Printf("combined out:\n%s\n", string(out))
				log.Fatalf("cmd.Run() failed with %s\n", err)
			} else {
				fmt.Printf("combined out:\n%s\n", string(out))
				Write(FilenameDB, []byte(filename))
				Write(FilenameDB, []byte("\n"))
				Dfile(zipname)
			}
			//fmt.Println(filename, e)
		} else if strings.HasSuffix(strings.ToUpper(fi.Name()), suffixzip) {
			fmt.Println("eeeeeee", filename)
			zipname := strings.Split(filename, ".")
			if IsDir(zipname[0]) == true {
				fmt.Println("zipname[7777777]", zipname[0])
				Dfile(zipname[0])
				return nil
			}
			cmd := exec.Command("mkdir", "-p", ""+zipname[0])
			out, err := cmd.CombinedOutput()
			if err != nil {
				fmt.Printf("combined out:\n%s\n", string(out))
				log.Fatalf("cmd.Run() failed with %s\n", err)
			} else {
				fmt.Println("haha", zipname[0], filename)
				cmds := exec.Command("unzip", "-n", "-d", zipname[0], filename)
				out, err := cmds.CombinedOutput()
				if err != nil {
					Write("err", []byte(err.Error()))
					Write("err", []byte("\n"))
					Write("err-msg", out)
					Write("err-msg", []byte("\n"))
					return nil
					fmt.Printf("combined out:\n%s\n", string(out))
					log.Fatalf("cmd.Run() failed with %s\n", err)
				} else {
					fmt.Printf("combined out:\n%s\n", string(out))
					Write(Filename, []byte(filename))
					Write(Filename, []byte("\n"))
					Dfile(zipname[0])
				}
			}
		}
		return nil
	})
	return err
}

func CheckBook(s string) (book NBook, err error) {
	err = DB.Table("nbook").Select("nid").Where("book LIKE ?", "%"+s+"%").Find(&book).Error
	return book, err
}

func CheckNid(s string) (book NBook, err error) {
	err = DB.Table("nbook").Select("status").Where("nid = ?", s).Find(&book).Error
	return book, err
}

func UpdateDB(b NBook) error {
	nid, _ := CheckNid(b.Nid)
	if nid.Status == 1 {
		return errors.New("this nid already exist")
	} else {
		fmt.Println("b.Filepath", b)
		return DB.Table("nbook").Where("nid = ?", b.Nid).Updates(NBook{Status: 1, Filepath: b.Filepath}).Error
	}
	//fmt.Println("b.Filepath", b)
	//return DB.Table("nbook").Where("nid = ?", b.Nid).Updates(NBook{Status: 1, Filepath: b.Filepath}).Error
}

func Write(s string, data []byte) error {
	file, errs := os.OpenFile(s, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
	if errs != nil {
		fmt.Printf("Open file error. err=%s", errs)
	}
	defer file.Close()
	_, err := file.Write(data)
	if err != nil {
		fmt.Printf("Write file error. err=%s", err)
		return err
	}
	return nil
}

func Writes(data []byte) error {
	_, err := File.Write(data)
	if err != nil {
		fmt.Printf("Write file error. err=%s", err)
		return err
	}
	return nil
}

func data() []string {
	s := []string{".cdr", ".ps", ".eps", ".fbd", ".PS", ".s92", ".rar", ".jpg", ".doc", ".FBD", ".s10", ".xls", ".bak", ".RED", ".bmp", ".tif", ".TXT"}
	return s
}

func MatchChineseForStr(str string, matchType int) string {
	r := []rune(str)
	cnStr := ""
	if matchType == 1 {
		for i := 0; i < len(r); i++ {
			if r[i] <= 40869 && r[i] >= 19968 {
				cnStr = cnStr + string(r[i])
			}
		}
		if cnStr == "" {
			return str
		}
		return cnStr
	} else {
		//提取第一个中文到结尾
		beginIndex := false
		for i := 0; i < len(r); i++ {
			if beginIndex == false {
				if r[i] <= 40869 && r[i] >= 19968 {
					beginIndex = true
				}
			}
			if beginIndex {
				cnStr += string(r[i])
			}
		}
		//去掉尾部非中文
		waitRune := []rune(cnStr)
		waitRes := ""
		endIndex := false
		for i := len(waitRune) - 1; i >= 0; i-- {
			if endIndex == false {
				if waitRune[i] <= 40869 && waitRune[i] >= 19968 {
					endIndex = true
				}
			}
			if endIndex {
				waitRes += string(waitRune[i])
			}
		}
		//翻转
		lastStr := ""
		lastRune := []rune(waitRes)
		for i := len(lastRune) - 1; i >= 0; i-- {
			lastStr += string(lastRune[i])
		}
		if lastStr == "" {
			return str
		}
		return lastStr
	}
}

func MatchChineseForDB(s string) string {
	var ss string
	if strings.Contains(s, ".") == true {
		split := strings.Split(s, ".")
		ss = split[0]
	} else if strings.Contains(ss, "（") {
		split := strings.Split(s, "（")
		ss = split[0]
	} else if strings.Contains(ss, "(") {
		split := strings.Split(s, "(")
		ss = split[0]
	} else if strings.Contains(ss, "、") {
		split := strings.Split(s, "、")
		ss = split[1]
	} else if strings.Contains(ss, "《") {
		MatchChineseForStr(ss, 1)
	}
	return ss
}

func InitELK() {
	cfg := elkproducer.Config{
		Addresses: []string{
			"http://10.2.0.21:9200",
		},
		Username: "elastic",
		Password: "elastic",
	}
	configelk := elkproducer.ESConfig{
		ESConf: cfg,
		//IndexType: "log",
		DebugMode: false,
		Index:     "test2",
	}
	es, err := elkproducer.NewClient(configelk)
	if err != nil {
		fmt.Println("first elkproducer.NewClient", err)
	} else {
		Es = es
	}
	//nbook := GetNbook()
	//if len(nbook) > 0 {
	//	for _, v := range nbook {
	//		Es.AddLog(v)
	//		return
	//	}
	//}
}

func GetNbook() []NBook {
	var nbooks []NBook
	DB.Table("nbook").Find(&nbooks)
	return nbooks
}

func ESnid(s string) (NBook, bool) {
	query := map[string]interface{}{
		"query": map[string]interface{}{
			"term": map[string]interface{}{
				"nid": s,
			},
		},
	}
	res := Es.GetData(query)
	var b NBook
	if res["hits"].(map[string]interface{})["total"].(map[string]interface{})["value"].(float64) == 1 {
		b.Nid = res["hits"].(map[string]interface{})["hits"].([]interface{})[0].(map[string]interface{})["_source"].(map[string]interface{})["nid"].(string)
		return b, true
	} else {
		return b, false
	}
}

func ESbook(s string) (NBook, bool) {
	query := map[string]interface{}{
		"query": map[string]interface{}{
			"wildcard": map[string]interface{}{
				"book.keyword": "*" + s + "*",
			},
		},
	}
	var b NBook
	res := Es.GetData(query)
	if res["hits"].(map[string]interface{})["total"].(map[string]interface{})["value"].(float64) == 1 {
		b.Nid = res["hits"].(map[string]interface{})["hits"].([]interface{})[0].(map[string]interface{})["_source"].(map[string]interface{})["nid"].(string)
		return b, true
	} else {
		return b, false
	}
}

func IsDir(fileAddr string) bool {
	s, err := os.Stat(fileAddr)
	if err != nil {
		log.Println("lala", err)
		return false
	}
	return s.IsDir()
}

func IsFile(fileAddr string) bool {
	_, err := os.Stat(fileAddr)
	if err != nil {
		log.Println("lala", err)
		return false
	} else {
		return true
	}
}

func MakeFile(s string) {
	cmd := exec.Command("mkdir", "-p", s)
	out, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println("333")
		Write("mk-err", []byte(err.Error()))
		Write("mk-err", []byte("\n"))
		Write("mk-err-msg", out)
		Write("mk-err-msg", []byte("\n"))
		return
		fmt.Printf("combined out:\n%s\n", string(out))
		log.Fatalf("cmd.Run() failed with %s\n", err)
	}
}

func MVFile(s, d string) {
	cmd := exec.Command("mv", s, d)
	out, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println("12-12")
		Write("mv-err", []byte(err.Error()))
		Write("mv-err", []byte("\n"))
		Write("mv-err-msg", out)
		Write("mv-err-msg", []byte("\n"))
	} else {
		fmt.Println("13-13")
		fmt.Printf("combined out:\n%s\n", string(out))
		Write("succ", []byte(s))
		Write("succ", []byte("\n"))
	}
}

func RAR(s, d string) error {
	cmd := exec.Command("unrar", "x", "-o-", "-y", ""+s, d+"/")
	out, err := cmd.CombinedOutput()
	if err != nil {
		Write("err", []byte(err.Error()))
		Write("err", []byte("\n"))
		Write("err-msg", out)
		Write("err-msg", []byte("\n"))
		Write("err-book", []byte(s))
		Write("err-book", []byte("\n"))
		fmt.Printf("combined out:\n%s\n", string(out))
		log.Fatalf("cmd.Run() failed with %s\n", err)
		return err
	} else {
		fmt.Printf("combined out:\n%s\n", string(out))
		Write(FilenameDB, []byte(s))
		Write(FilenameDB, []byte("\n"))
		return nil
	}
}

func ZIP(s, d string) error {
	cmds := exec.Command("unzip", "-n", "-d", d, s)
	out, err := cmds.CombinedOutput()
	if err != nil {
		Write("err", []byte(err.Error()))
		Write("err", []byte("\n"))
		Write("err-msg", out)
		Write("err-msg", []byte("\n"))
		Write("err-book", []byte(s))
		Write("err-book", []byte("\n"))
		fmt.Printf("combined out:\n%s\n", string(out))
		log.Fatalf("cmd.Run() failed with %s\n", err)
		return err
	} else {
		fmt.Printf("combined out:\n%s\n", string(out))
		Write(Filename, []byte(s))
		Write(Filename, []byte("\n"))
		return nil
	}
}

func TAR(s, d string) error {
	cmd := exec.Command("tar", "-xzvf", s, "-C", d)
	out, err := cmd.CombinedOutput()
	if err != nil {
		Write("err", []byte(err.Error()))
		Write("err", []byte("\n"))
		Write("err-msg", out)
		Write("err-msg", []byte("\n"))
		Write("err-book", []byte(s))
		Write("err-book", []byte("\n"))
		fmt.Printf("combined out:\n%s\n", string(out))
		log.Fatalf("cmd.Run() failed with %s\n", err)
		return err

	} else {
		fmt.Printf("combined out:\n%s\n", string(out))
		Write(FilenameDB, []byte(s))
		Write(FilenameDB, []byte("\n"))
		return nil

	}
}

func LNS(s, d string) {
	cmd := exec.Command("ln", "-s", s+"/", d)
	out, err := cmd.CombinedOutput()
	if err != nil {
		Write("err", []byte(err.Error()))
		Write("err", []byte("\n"))
		Write("ln-msg", out)
		Write("ln-msg", []byte("\n"))
		Write("ln-book", []byte(s))
		Write("ln-book", []byte("\n"))
		return
		fmt.Printf("combined out:\n%s\n", string(out))
		log.Fatalf("cmd.Run() failed with %s\n", err)
	} else {
		fmt.Printf("combined out:\n%s\n", string(out))
		Write("ln", []byte(s))
		Write("ln", []byte("\n"))
		return
	}
}

//var i In
//i.Index.Index = "test2"
//marshal, _ := json.Marshal(i)
//var s string
//for k, v := range GetNbook() {
//	if k >= 0 && k < 100000 {
//		s = "0.json"
//		Write(s, marshal)
//		Write(s, []byte("\n"))
//		mv, _ := json.Marshal(v)
//		Write(s, mv)
//		Write(s, []byte("\n"))
//	} else if k >= 100000 && k < 200000 {
//		s = "10.json"
//		Write(s, marshal)
//		Write(s, []byte("\n"))
//		mv, _ := json.Marshal(v)
//		Write(s, mv)
//		Write(s, []byte("\n"))
//	} else if k >= 200000 && k < 300000 {
//		s = "20.json"
//		Write(s, marshal)
//		Write(s, []byte("\n"))
//		mv, _ := json.Marshal(v)
//		Write(s, mv)
//		Write(s, []byte("\n"))
//	} else if k >= 300000 && k < 400000 {
//		s = "30.json"
//		Write(s, marshal)
//		Write(s, []byte("\n"))
//		mv, _ := json.Marshal(v)
//		Write(s, mv)
//		Write(s, []byte("\n"))
//	} else if k >= 400000 && k < 500000 {
//		s = "40.json"
//		Write(s, marshal)
//		Write(s, []byte("\n"))
//		mv, _ := json.Marshal(v)
//		Write(s, mv)
//		Write(s, []byte("\n"))
//	} else if k >= 500000 && k < 600000 {
//		s = "50.json"
//		Write(s, marshal)
//		Write(s, []byte("\n"))
//		mv, _ := json.Marshal(v)
//		Write(s, mv)
//		Write(s, []byte("\n"))
//	} else if k >= 600000 && k < 700000 {
//		s = "60.json"
//		Write(s, marshal)
//		Write(s, []byte("\n"))
//		mv, _ := json.Marshal(v)
//		Write(s, mv)
//		Write(s, []byte("\n"))
//	} else if k >= 700000 && k < 800000 {
//		s = "70.json"
//		Write(s, marshal)
//		Write(s, []byte("\n"))
//		mv, _ := json.Marshal(v)
//		Write(s, mv)
//		Write(s, []byte("\n"))
//	} else if k >= 800000 && k < 900000 {
//		s = "80.json"
//		Write(s, marshal)
//		Write(s, []byte("\n"))
//		mv, _ := json.Marshal(v)
//		Write(s, mv)
//		Write(s, []byte("\n"))
//	} else {
//		s = "90.json"
//		Write(s, marshal)
//		Write(s, []byte("\n"))
//		mv, _ := json.Marshal(v)
//		Write(s, mv)
//		Write(s, []byte("\n"))
//	}
//}
//InitELK()
//return
